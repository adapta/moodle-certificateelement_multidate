<?php
// This file is part of the tool_certificate plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace certificateelement_multidate;

require_once($CFG->dirroot . '/lib/grade/constants.php');

/**
 * The certificate element multidate's core interaction API.
 *
 * @package    certificateelement_multidate
 * @copyright  2023 Daniel Neis Araujo
 * @copyright  2013 Mark Nelson <markn@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class element extends \tool_certificate\element {

    /**
     * @var int Show creation multidate
     */
    const CUSTOMCERT_DATE_ISSUE = -1;

    /**
     * @var int Show expiry multidate.
     */
    const CUSTOMCERT_DATE_EXPIRY = -2;

    /**
     * This function renders the form elements when adding a certificate element.
     *
     * @param \MoodleQuickForm $mform the edit_form instance
     */
    public function render_form_elements($mform) {

        // Get the possible multidate options.
        $multidateoptions = [];
        $multidateoptions[self::CUSTOMCERT_DATE_ISSUE] = get_string('issuedmultidate', 'certificateelement_multidate');
        $multidateoptions[self::CUSTOMCERT_DATE_EXPIRY] = get_string('expirymultidate', 'certificateelement_multidate');

        $mform->addElement('select', 'multidateitem', get_string('multidateitem', 'certificateelement_multidate'), $multidateoptions);
        $mform->addHelpButton('multidateitem', 'multidateitem', 'certificateelement_multidate');

        $mform->addElement('select', 'multidateformat', get_string('multidateformat', 'certificateelement_multidate'), self::get_date_formats());
        $mform->addHelpButton('multidateformat', 'multidateformat', 'certificateelement_multidate');

        $mform->addElement('text', 'multidatedaysbefore', get_string('multidatedaysbefore', 'certificateelement_multidate'), 0);
        $mform->setType('multidatedaysbefore', PARAM_INT);

        $mform->addElement('text', 'multidatedaysafter', get_string('multidatedaysafter', 'certificateelement_multidate'), 0);
        $mform->setType('multidatedaysafter', PARAM_INT);

        parent::render_form_elements($mform);
    }

    /**
     * Handles saving the form elements created by this element.
     * Can be overridden if more functionality is needed.
     *
     * @param \stdClass $data the form data or partial data to be upmultidated (i.e. name, posx, etc.)
     */
    public function save_form_data(\stdClass $data) {
        $data->data = json_encode([
            'multidateitem' => $data->multidateitem,
            'multidateformat' => $data->multidateformat,
            'multidatedaysafter' => $data->multidatedaysafter,
            'multidatedaysbefore' => $data->multidatedaysbefore,
        ]);
        parent::save_form_data($data);
    }

    /**
     * Handles rendering the element on the pdf.
     *
     * @param \pdf $pdf the pdf object
     * @param bool $preview true if it is a preview, false otherwise
     * @param \stdClass $user the user we are rendering this for
     * @param \stdClass $issue the issue we are rendering
     */
    public function render($pdf, $preview, $user, $issue) {
        // Decode the information stored in the database.
        $multidateinfo = @json_decode($this->get_data(), true) + ['multidateitem' => '', 'multidateformat' => ''];

        // If we are previewing this certificate then just show a demonstration multidate.
        if ($preview) {
            $multidate = time();
        } else if ($multidateinfo['multidateitem'] == self::CUSTOMCERT_DATE_EXPIRY) {
            $multidate = $issue->expires;
        } else {
            $multidate = $issue->timecreated;
        }

        if ($multidateinfo['multidatedaysbefore'] > 0) {
            for ($i = $multidateinfo['multidatedaysbefore']; $i > 0; $i--) {
                $dates[] = $this->get_date_format_string(strtotime("-{$i} days"), $multidateinfo['multidateformat']);
            }
        }
        $dates[] = $this->get_date_format_string($multidate, $multidateinfo['multidateformat']);

        if ($multidateinfo['multidatedaysafter'] > 0) {
            for ($i = 1; $i <= $multidateinfo['multidatedaysafter']; $i++) {
                $dates[] = $this->get_date_format_string(strtotime("+{$i} days"), $multidateinfo['multidateformat']);
            }
        }

        // Ensure that a multidate has been set.
        if (!empty($multidate)) {
            \tool_certificate\element_helper::render_content($pdf, $this, implode(', ', $dates));
        }
    }

    /**
     * Render the element in html.
     *
     * This function is used to render the element when we are using the
     * drag and drop interface to position it.
     *
     * @return string the html
     */
    public function render_html() {
        // Decode the information stored in the database.
        $multidateinfo = @json_decode($this->get_data(), true) + ['multidateformat' => ''];
        $dates = [];

        if ($multidateinfo['multidatedaysbefore'] > 0) {
            for ($i = $multidateinfo['multidatedaysbefore']; $i > 0; $i--) {
                $dates[] = $this->get_date_format_string(strtotime("-{$i} days"), $multidateinfo['multidateformat']);
            }
        }

        $dates[] = $this->get_date_format_string(time(), $multidateinfo['multidateformat']);

        if ($multidateinfo['multidatedaysafter'] > 0) {
            for ($i = 1; $i <= $multidateinfo['multidatedaysafter']; $i++) {
                $dates[] = $this->get_date_format_string(strtotime("+{$i} days"), $multidateinfo['multidateformat']);
            }
        }
        return \tool_certificate\element_helper::render_html_content($this, implode(', ', $dates));
    }

    /**
     * Prepare data to pass to moodleform::set_data()
     *
     * @return \stdClass|array
     */
    public function prepare_data_for_form() {
        $record = parent::prepare_data_for_form();
        if (!empty($this->get_data())) {
            $multidateinfo = json_decode($this->get_data());
            $record->multidateitem = $multidateinfo->multidateitem;
            $record->multidateformat = $multidateinfo->multidateformat;
            $record->multidatedaysafter = $multidateinfo->multidatedaysafter;
            $record->multidatedaysbefore = $multidateinfo->multidatedaysbefore;
        }
        return $record;
    }

    /**
     * Helper function to return all the multidate formats.
     *
     * @return array the list of multidate formats
     */
    public static function get_date_formats() {
        // Hard-code date so users can see the difference between short dates with and without the leading zero.
        // Eg. 06/07/18 vs 6/07/18.
        $multidate = 1530849658;

        $multidateformats = [];

        $strmultidateformats = [
            'strftimedate',
            'strftimedatefullshort',
            'strftimedatefullshortwleadingzero',
            'strftimedateshort',
            'strftimedaydate',
            'strftimedayshort',
            'strftimemonthyear',
        ];

        foreach ($strmultidateformats as $strmultidateformat) {
            $multidateformats[$strmultidateformat] = self::get_date_format_string($multidate, $strmultidateformat);
        }

        return $multidateformats;
    }

    /**
     * Returns the multidate in a readable format.
     *
     * @param int $multidate
     * @param string $multidateformat
     * @return string
     */
    protected static function get_date_format_string($multidate, $multidateformat) {
        if ($multidateformat == 'strftimedatefullshortwleadingzero') {
            $certificatemultidate = userdate($multidate, get_string('strftimedatefullshort', 'langconfig'), 99, false);
        } else if (get_string_manager()->string_exists($multidateformat, 'langconfig')) {
            $certificatemultidate = userdate($multidate, get_string($multidateformat, 'langconfig'));
        } else {
            $certificatemultidate = userdate($multidate, get_string('strftimedate', 'langconfig'));
        }
        return $certificatemultidate;
    }
}
